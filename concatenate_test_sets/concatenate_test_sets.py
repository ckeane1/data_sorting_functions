

import os
import pandas as pd

def gen_csvs():
    """Concatenate csvs from a test set into a single test csv.
    Args:
        none
    Returns:
        Generates a single CSV out of a file containing a set of cronological CSVS
    """
    index = ['Timestamp']

    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'ECU Pitch Rate','ECU Yaw Rate','ECU Roll Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu_sigs = ['Pusher EPU A Commanded Torque', 'Pusher EPU B Motor Temperature 1', 'Pusher EPU B Motor Temperature 2',
            'Pusher EPU B Commanded Torque', 'Pusher EPU A Velocity', 'Pusher EPU B Velocity',
            'Pusher EPU A Motor Temperature 1', 'Pusher EPU A Motor Temperature 2',]
    usecols = index  + velocities + positions + angles + latlong + epu_sigs
    ## CSV PATHS || PUT EACH SET OF CSVS INTO THEIR OWN DIRECTORY INSIDE THE DIRECTORY "raw_csvs"

    csvpath = os.path.dirname(os.path.realpath(__file__))+"/raw_csvs"

    csv_sets_list = os.listdir(csvpath) # List of files containing test sets
    ## CONCATENATE SPLIT CSVS INTO WHOLE ONES ON A PER SET BASIS
    print( "Found " + str(len(csv_sets_list)) + " test sets!")

    test_sum = 0
    ## Run for the number of sets in the directory
    for set in csv_sets_list:
        set_path = os.path.dirname(os.path.realpath(__file__)) + "/raw_csvs/" + set 
        set_contents = os.listdir(set_path)
        frames = []
        df_ = pd.DataFrame()
        ## Run for each test in the set
        for test in set_contents:
            df_ = pd.read_csv('raw_csvs/'+ set + "/" + test, usecols = usecols)
            frames.append(df_)
        df = pd.concat(frames)
        df.to_csv('usable_csvs/' + set + '_whole')

        test_sum = test_sum + len(set_contents)



    print("Done! Concatenated " + str(test_sum) + " tests into " + str(len(csv_sets_list)) + " whole set csvs!")


if __name__ == '__main__':
    gen_csvs()