import os
import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px



# POSITIONAL LIMITS
GPS_ABSOLUTE_MIN = 0 # feet || RESET IN THE FUNCTION BASED ON MINIMUM ALTITUDE
# The following are with respect to the altitude of the airstrip which is set in each function
GPS_PATTERN_ALTITUDE = 1300 + GPS_ABSOLUTE_MIN # feet
GPS_OBSTACLE_CLEARANCE_ALTITUDE = 50 + GPS_ABSOLUTE_MIN # feet

# VELOCITY LIMITS
AIRSPEED_TAKEOFF_MAX = 90 # knots
AIRSPEED_TAKEOFF_MIN = 85 # knots
AIRSPEED_TAXI_MAX = 20 # knots
AIRSPEED_TAXI_MIN = 6 # knots
CLIMB_RATE_MIN = 1.016 # FPM

def pattern_points(path) -> pd.DataFrame:
    """Find and return a dataframe of the flight points below pattern for this flight.
    Args:
        path (str): path to flight test dataframe
    Returns:
        pd.DataFrame: dataframe containing cruise points for this flight
    """
    # variable setup
    index = ['Timestamp']

    ## Pull in only the necessary and relevant variables from the CSVs to save on compute time
    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'ECU Pitch Rate','ECU Yaw Rate','ECU Roll Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu_sigs = ['Pusher EPU A Commanded Torque', 'Pusher EPU B Motor Temperature 1', 'Pusher EPU B Motor Temperature 2',
                'Pusher EPU B Commanded Torque', 'Pusher EPU A Velocity', 'Pusher EPU B Velocity',
                'Pusher EPU A Motor Temperature 1', 'Pusher EPU A Motor Temperature 2',]

    ## Define sets of the variables depending on where they are going
    usecols = index + velocities + positions + angles + latlong + epu_sigs
    differentiables = velocities + positions + angles

    ## Read in CSV to DataFrame
    df = pd.read_csv(path, usecols=usecols)

    ## Convert timestrings under column 'Timestamp' to datetime64 under 'Datetime' and seconds elapsed under 'Elapsed'
    df['Datetime'] = pd.to_datetime(df['Timestamp'], format='%H:%M:%S:%f')
    df.set_index('Datetime', inplace=True)
    ## Resample data to a 1s period using the (mean) of each period
    df = df.resample('1s').mean()
    df['Elapsed'] = (df.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1ns')
    df['Elapsed'] = (df['Elapsed'] - df['Elapsed'].iloc[0]) / 10**9

    ## Run a simple differentiation on the column list (differentiables)
    for column in differentiables:
        df[column + "_differential"] = (df[column].diff(periods=1) / df['Elapsed'].diff(periods=1))

    ## Reset airstrip specfic filters

    ## Primary filter to remove discontinuities like zeros or NaNs from the dataframe
    GPS_ABSOLUTE_MIN = df['GPS Altitude'].min() 
    gps_cutoff = df['GPS Altitude'] > GPS_ABSOLUTE_MIN

    df = df[gps_cutoff]

    ## Secondary filter to cut the data frame to just sections below pattern altitude
    GPS_UPPER = df['GPS Altitude'].min() + 1300

    # SET POSITIONAL FILTERS
    gps_upper_obstacle = df['GPS Altitude'] < GPS_UPPER

    # SET VELOCITY FILTERS

    pattern_points = df[ (gps_upper_obstacle) ]

    pattern_points['Time Difference'] = pattern_points['Elapsed'].diff()
    
    ground_speed_list = []
    seconds_elapsed_list = []

    has_found_pattern = False

    i = 0
    for index, row in pattern_points.iterrows():

        i = i + 1
        if row['Time Difference'] <= 3:
            ground_speed_list.append(row['Ground Speed']*3.2808399)
            seconds_elapsed_list.append(row['Elapsed'])
        
        if ((row['Time Difference']) > 3) and (len(seconds_elapsed_list) > 20):
            point_distance = np.trapz(ground_speed_list)
            point_time = (seconds_elapsed_list[-1] - seconds_elapsed_list[0])

            # Select the takeoff and landing distances
            if point_time > 50 & has_found_pattern== False:
                pattern_distance = point_distance
                pattern_time = point_time
                has_found_pattern = False

            ground_speed_list = []
            seconds_elapsed_list = []

    return (pattern_points,pattern_distance,pattern_time)
    

def takeoff_ground_roll_points(pattern_points) -> pd.DataFrame:
    """Calculate Ground Roll Distance and times for specific Pattern points.
    Args:
        pattern_points (pd.DataFrame): dataframe flight points below pattern
    Returns:
        pd.DataFrame: dataframe of just L/D points with L/D as a column
    """

    ## Primary filter to remove discontinuities like zeros or NaNs from the dataframe
    # GPS_ABSOLUTE_MIN = pattern_points['GPS Altitude'].min() 

    # gps_cutoff = pattern_points['GPS Altitude'] > GPS_ABSOLUTE_MIN

    # pattern_points = pattern_points[(gps_cutoff)]

    ## Secondary filter to cut the data frame to just sections below takeoff speed but above taxi speed
    GPS_AIRSTRIP_ALTITUDE = pattern_points['GPS Altitude'].min()  ## Set new minimum to be the airstrip altitude
    
    ## DEFINE CONSTANTS OF MARGINS
    GPS_GROUNDROLL_MAX = GPS_AIRSTRIP_ALTITUDE + 20
    GPS_GROUNDROLL_MIN = GPS_AIRSTRIP_ALTITUDE - 20

    ## TRANSLATE INTO FILTER ARGUMENTS

    gps_upper_groundroll = pattern_points['GPS Altitude'] < GPS_GROUNDROLL_MAX
    gps_lower_groundroll = pattern_points['GPS Altitude'] > GPS_GROUNDROLL_MIN
    true_airspeed_upper = pattern_points['True Airspeed'] < AIRSPEED_TAKEOFF_MIN
    true_airspeed_lower = pattern_points['True Airspeed'] > AIRSPEED_TAXI_MAX 

    ## APPLY FILTER
    ground_roll_points = pattern_points[ (gps_upper_groundroll) & (gps_lower_groundroll) & (true_airspeed_upper) & (true_airspeed_lower)]

    ## USE TIME DIFFERENCING TO DIFFERENTIATE DISTINCT SECTIONS OF FILTERED DATA
    ground_roll_points = ground_roll_points.reset_index()
    ground_roll_points['Time Difference'] = ground_roll_points['Elapsed'].diff()

    ## CONSTANTS AND LISTS

    has_found_takeoff = False
    has_found_landing = False

    ground_speed_list = []
    seconds_elapsed_list = []

    takeoff_ground_roll_distance = 0
    takeoff_ground_roll_time = 0
    landing_ground_roll_distance = 0
    landing_ground_roll_time = 0
    i = 0

    TIME_DIFFERENCE_MIN = 25

    test_total_time = pattern_points['Elapsed'].max()
    print("Total Time", test_total_time)

    for index, row in ground_roll_points.iterrows():

        i = i + 1

        ## COLLECT THE VALUES OF VARIABLES USED IN CALCULATIONS
        if row['Time Difference'] <= TIME_DIFFERENCE_MIN:
            ground_speed_list.append(row['Ground Speed']*3.2808399)
            seconds_elapsed_list.append(row['Elapsed'])

        ## STOP COLLECTING WHEN A TIME DIFFERENCE IS FOUND AND PERFORM A CALCULATION
        if ((row['Time Difference']) > TIME_DIFFERENCE_MIN) and (len(seconds_elapsed_list) > 4):
            len(seconds_elapsed_list)
            point_distance = np.trapz(ground_speed_list)
            point_start_time = seconds_elapsed_list[0]
            point_end_time = seconds_elapsed_list[-1]
            point_time = (point_end_time - point_start_time)
            print("POTENTIAL Takeoff GROUND ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])
           ## SELECT THE MOST VIABLE GROUP OF DATA AS THE TRUE SET

            print(point_time,point_distance,seconds_elapsed_list[0])
            if  (500 > point_time > 5) and (8000 > point_distance > 1000) & ((point_end_time) < (test_total_time*0.5)):
                takeoff_ground_roll_distance = point_distance
                takeoff_ground_roll_time = point_time
                has_found_takeoff = False
                print("SELECTED Takeoff GROUND ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])
            ground_speed_list = []
            seconds_elapsed_list = []
    return (ground_roll_points, takeoff_ground_roll_distance, takeoff_ground_roll_time)


def landing_ground_roll_points(pattern_points) -> pd.DataFrame:
    """Calculate Ground Roll Distance and times for specific Pattern points.
    Args:
        pattern_points (pd.DataFrame): dataframe flight points below pattern
    Returns:
        pd.DataFrame: dataframe of just L/D points with L/D as a column
    """

    ## Primary filter to remove discontinuities like zeros or NaNs from the dataframe
    # GPS_ABSOLUTE_MIN = pattern_points['GPS Altitude'].min() 

    # gps_cutoff = pattern_points['GPS Altitude'] > GPS_ABSOLUTE_MIN

    # pattern_points = pattern_points[(gps_cutoff)]

    ## Secondary filter to cut the data frame to just sections below takeoff speed but above taxi speed
    GPS_AIRSTRIP_ALTITUDE = pattern_points['GPS Altitude'].min()  ## Set new minimum to be the airstrip altitude
    TEST_LENGTH_SECONDS = test_total_time = pattern_points['Elapsed'].max()
    ## DEFINE CONSTANTS OF MARGINS
    GPS_GROUNDROLL_MAX = GPS_AIRSTRIP_ALTITUDE + 20
    GPS_GROUNDROLL_MIN = GPS_AIRSTRIP_ALTITUDE - 20

    ## TRANSLATE INTO FILTER ARGUMENTS

    gps_upper_groundroll = pattern_points['GPS Altitude'] < GPS_GROUNDROLL_MAX
    gps_lower_groundroll = pattern_points['GPS Altitude'] > GPS_GROUNDROLL_MIN 
    true_airspeed_upper = pattern_points['True Airspeed'] < AIRSPEED_TAKEOFF_MIN
    true_airspeed_lower = pattern_points['True Airspeed'] > AIRSPEED_TAXI_MAX 
    time_elapsed_upper = pattern_points["Elapsed"] < TEST_LENGTH_SECONDS
    time_elapsed_lower = pattern_points["Elapsed"] > (TEST_LENGTH_SECONDS * 0.5)

    ## APPLY FILTER
    ground_roll_points = pattern_points[ (time_elapsed_upper) & (time_elapsed_lower) & (gps_upper_groundroll) & (gps_lower_groundroll) & (true_airspeed_upper) & (true_airspeed_lower)]

    ## USE TIME DIFFERENCING TO DIFFERENTIATE DISTINCT SECTIONS OF FILTERED DATA
    ground_roll_points = ground_roll_points.reset_index()
    ground_roll_points['Time Difference'] = ground_roll_points['Elapsed'].diff()

    ## CONSTANTS AND LISTS

    has_found_takeoff = False
    has_found_landing = False

    ground_speed_list = []
    seconds_elapsed_list = []

    takeoff_ground_roll_distance = 0
    takeoff_ground_roll_time = 0
    landing_ground_roll_distance = 0
    landing_ground_roll_time = 0
    i = 0

    TIME_DIFFERENCE_MIN = 10


    print("Total Time", test_total_time)

    for index, row in ground_roll_points.iterrows():

        i = i + 1

        ## COLLECT THE VALUES OF VARIABLES USED IN CALCULATIONS
        if row['Time Difference'] <= TIME_DIFFERENCE_MIN:
            ground_speed_list.append(row['Ground Speed']*3.2808399)
            seconds_elapsed_list.append(row['Elapsed'])


        if (row['Time Difference'] > TIME_DIFFERENCE_MIN) or (index == ground_roll_points.index[-1]):

            point_start_time = seconds_elapsed_list[0]
            point_end_time = seconds_elapsed_list[-1]
            point_time = (point_end_time - point_start_time)

            time_cap = ground_roll_points["Elapsed"] < point_end_time
            ground_roll_points = ground_roll_points[(time_cap)]

        ## STOP COLLECTING WHEN A TIME DIFFERENCE IS FOUND AND PERFORM A CALCULATION
            if (len(seconds_elapsed_list) > 20):

                point_distance = np.trapz(ground_speed_list)

                print("POTENTIAL LANDING ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed end", point_end_time)
                
                ## SELECT THE MOST VIABLE GROUP OF DATA AS THE TRUE SET
                if ( 400 > point_time > 20) and ( 12000 > point_distance > 2000) or (index == ground_roll_points.index[-1]):
                    landing_ground_roll_distance = point_distance
                    landing_ground_roll_time = point_time
                    has_found_landing= True
                    print("SELECTED LANDING ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])
                    # print("Landing GROUND ROLL landing - Distance", point_distance,"Time", point_time, "Elapsed", seconds_elapsed_list[0])
            ground_speed_list = [] 
            seconds_elapsed_list = []

    return (ground_roll_points, landing_ground_roll_distance, landing_ground_roll_time)



############
## MAIN
###########

## RUN THE TAKEOFF AND LANDING PARSER ON THE WHOLE CSVS
csvpath = os.path.dirname(os.path.realpath(__file__))+"/usable_csvs"

csv_list = os.listdir(csvpath) # List of files containing test sets

print( "Found " + str(len(csv_list)) + " tests")

test_sum = 0

nameout = "takeoff_and_landing_data.csv"

set_list = []
takeoff_roll_distances_list = []
takeoff_roll_times_list = []
takeoff_obstacle_clearance_distances_list = []
takeoff_obstacle_clearance_times_list = []
landing_roll_distances_list = []
landing_roll_times_list = []
landing_obstacle_clearance_distances_list = []
landing_obstacle_clearance_times_list = []
pattern_distance_list = []
pattern_times_list = []


low_torque_climb_rates_total = pd.DataFrame()
high_torque_climb_rates_total = pd.DataFrame()

for set in csv_list:

    print("Reading in", set, "..." )

    pattern_points_output = pattern_points(csvpath + "/" +set) 

    # obstacle_clearance_points_output = obstacle_clearance_points(pattern_points_output[0])
    
    takeoff_ground_roll_points_output = takeoff_ground_roll_points(pattern_points_output[0])
    landing_ground_roll_points_output = landing_ground_roll_points(pattern_points_output[0])

    # low_torque_climb_rates_total = pd.concat(low_torque_climb_rates_total,climb_points_output[0])
    # fig = make_subplots(rows=5, cols=1)
    # fig.update_layout(title_text= set)
    # fig.add_trace(go.Scatter(x=pattern_points_output[0]['Elapsed'], y=pattern_points_output[0]['Longitude'], mode='markers', name='long'), row=1, col=1)
    # fig.add_trace(go.Scatter(x=pattern_points_output[0]['Elapsed'], y=pattern_points_output[0]['GPS Altitude'], mode='markers', name='alt'), row=2, col=1)
    # fig.add_trace(go.Scatter(x=pattern_points_output[0]['Elapsed'], y=pattern_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed'), row=3, col=1)
    # fig.add_trace(go.Scatter(x=ground_roll_points_output[0]['Elapsed'], y=ground_roll_points_output[0]['Latitude'], mode='markers', name='Lat'), row=4, col=1)
    # fig.add_trace(go.Line(x=pattern_points_output[0]['Elapsed'], y=(-1*pattern_points_output[0]['Pusher EPU A Commanded Torque']+-1*pattern_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='ground acceleration'), row=5, col=1)

    # fig.show()

    # fig3 = make_subplots(rows=5, cols=1)
    # fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['Longitude'], mode='markers', name='long'), row=1, col=1)
    # fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['GPS Altitude'], mode='markers', name='alt'), row=2, col=1)
    # fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed'), row=3, col=1)
    # fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['Latitude'], mode='markers', name='Lat'), row=4, col=1)
    # fig3.add_trace(go.Line(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=(-1*takeoff_ground_roll_points_output[0]['Pusher EPU A Commanded Torque']+-1*takeoff_ground_roll_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='ground acceleration'), row=5, col=1)

    # fig3.show()


    output_dataframe = pd.DataFrame()

    low_torque_climb_rates_total.to_csv("low_torque_climb_rates_total" + set + ".csv")
    high_torque_climb_rates_total.to_csv("high_torque_climb_rates_total" + set + ".csv")

    set_list.append(set)
    takeoff_roll_distances_list.append(takeoff_ground_roll_points_output[1])
    takeoff_roll_times_list.append(takeoff_ground_roll_points_output[2])
    # takeoff_obstacle_clearance_distances_list.append(obstacle_clearance_points_output[1])
    # takeoff_obstacle_clearance_times_list.append(obstacle_clearance_points_output[2])
    landing_roll_distances_list.append(landing_ground_roll_points_output[1])
    landing_roll_times_list.append(landing_ground_roll_points_output[2])
    # landing_obstacle_clearance_distances_list.append(ground_roll_points_output[3])
    # landing_obstacle_clearance_times_list.append(ground_roll_points_output[4])
    pattern_distance_list.append(pattern_points_output[1])
    pattern_times_list.append(pattern_points_output[2])

output_dataframe['Date of Test'] = set_list
output_dataframe['Takeoff - Ground Roll Distance'] = takeoff_roll_distances_list
output_dataframe['Takeoff - Ground Roll Time'] = takeoff_roll_times_list
# output_dataframe['Takeoff - Obstacle Clearance Distance'] = takeoff_obstacle_clearance_distances_list
# output_dataframe['Takeoff - Obstacle Clearance Time'] = takeoff_obstacle_clearance_times_list
output_dataframe['Landing - Ground Roll Distance'] = landing_roll_distances_list
output_dataframe['Landing - Ground Roll Time'] = landing_roll_times_list
# output_dataframe['Landing - Obstacle Clearance Distance'] = landing_obstacle_clearance_distances_list 
# output_dataframe['Landing - Obstacle Clearance Time'] = landing_obstacle_clearance_times_list
output_dataframe['Pattern  Distance'] = pattern_distance_list
output_dataframe['Pattern Time'] = pattern_times_list

output_dataframe.to_csv(nameout,index = False)



