import os
import pandas as pd
from rolling_distance import pattern_points
from rolling_distance import obstacle_clearance_points
from rolling_distance import takeoff_ground_roll_points
from rolling_distance import landing_ground_roll_points
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

## RUN THE TAKEOFF AND LANDING PARSER ON THE WHOLE CSVS
csvpath = os.path.dirname(os.path.realpath(__file__))+"/usable_csvs"

csv_list = os.listdir(csvpath) # List of files containing test sets

print( "Found " + str(len(csv_list)) + " tests")

test_sum = 0

nameout = "takeoff_and_landing_data.csv"

set_list = []
takeoff_roll_distances_list = []
takeoff_roll_times_list = []
takeoff_obstacle_clearance_distances_list = []
takeoff_obstacle_clearance_times_list = []
landing_roll_distances_list = []
landing_roll_times_list = []
landing_obstacle_clearance_distances_list = []
landing_obstacle_clearance_times_list = []
pattern_distance_list = []
pattern_times_list = []

for set in csv_list:

    print("Reading in", set, "..." )

    pattern_points_output = pattern_points(csvpath + "/" +set) 

    # obstacle_clearance_points_output = obstacle_clearance_points(pattern_points_output[0])
    
    takeoff_ground_roll_points_output = takeoff_ground_roll_points(pattern_points_output[0])
    landing_ground_roll_points_output = landing_ground_roll_points(pattern_points_output[0])

    # fig = make_subplots(rows=5, cols=1)
    # fig.update_layout(title_text= set)
    # fig.add_trace(go.Scatter(x=pattern_points_output[0]['Elapsed'], y=pattern_points_output[0]['Longitude'], mode='markers', name='long'), row=1, col=1)
    # fig.add_trace(go.Scatter(x=pattern_points_output[0]['Elapsed'], y=pattern_points_output[0]['GPS Altitude'], mode='markers', name='alt'), row=2, col=1)
    # fig.add_trace(go.Scatter(x=pattern_points_output[0]['Elapsed'], y=pattern_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed'), row=3, col=1)
    # fig.add_trace(go.Scatter(x=ground_roll_points_output[0]['Elapsed'], y=ground_roll_points_output[0]['Latitude'], mode='markers', name='Lat'), row=4, col=1)
    # fig.add_trace(go.Line(x=pattern_points_output[0]['Elapsed'], y=(-1*pattern_points_output[0]['Pusher EPU A Commanded Torque']+-1*pattern_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='ground acceleration'), row=5, col=1)

    # fig.show()

    # fig2 = make_subplots(rows=5, cols=1)
    # fig2.add_trace(go.Scatter(x=obstacle_clearance_points_output[0]['Elapsed'], y=obstacle_clearance_points_output[0]['Longitude'], mode='markers', name='long'), row=1, col=1)
    # fig2.add_trace(go.Scatter(x=obstacle_clearance_points_output[0]['Elapsed'], y=obstacle_clearance_points_output[0]['GPS Altitude'], mode='markers', name='alt'), row=2, col=1)
    # fig2.add_trace(go.Scatter(x=obstacle_clearance_points_output[0]['Elapsed'], y=obstacle_clearance_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed'), row=3, col=1)
    # fig2.add_trace(go.Scatter(x=obstacle_clearance_points_output[0]['Elapsed'], y=obstacle_clearance_points_output[0]['Ground Speed_differential'], mode='markers', name='ground acceleration'), row=4, col=1)
    # fig2.add_trace(go.Line(x=obstacle_clearance_points_output[0]['Elapsed'], y=(-1*obstacle_clearance_points_output[0]['Pusher EPU A Commanded Torque']+-1*obstacle_clearance_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='ground acceleration'), row=5, col=1)

    # fig2.show()
    
    fig3 = make_subplots(rows=5, cols=1)
    fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['Longitude'], mode='markers', name='long'), row=1, col=1)
    fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['GPS Altitude'], mode='markers', name='alt'), row=2, col=1)
    fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed'), row=3, col=1)
    fig3.add_trace(go.Scatter(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=takeoff_ground_roll_points_output[0]['Latitude'], mode='markers', name='Lat'), row=4, col=1)
    fig3.add_trace(go.Line(x=takeoff_ground_roll_points_output[0]['Elapsed'], y=(-1*takeoff_ground_roll_points_output[0]['Pusher EPU A Commanded Torque']+-1*takeoff_ground_roll_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='ground acceleration'), row=5, col=1)

    fig3.show()

    fig4 = make_subplots(rows=5, cols=1)
    fig4.add_trace(go.Scatter(x=landing_ground_roll_points_output[0]['Elapsed'], y=landing_ground_roll_points_output[0]['Longitude'], mode='markers', name='long'), row=1, col=1)
    fig4.add_trace(go.Scatter(x=landing_ground_roll_points_output[0]['Elapsed'], y=landing_ground_roll_points_output[0]['GPS Altitude'], mode='markers', name='alt'), row=2, col=1)
    fig4.add_trace(go.Scatter(x=landing_ground_roll_points_output[0]['Elapsed'], y=landing_ground_roll_points_output[0]['True Airspeed'], mode='markers', name='True Airspeed'), row=3, col=1)
    fig4.add_trace(go.Scatter(x=landing_ground_roll_points_output[0]['Elapsed'], y=landing_ground_roll_points_output[0]['Latitude'], mode='markers', name='Lat'), row=4, col=1)
    fig4.add_trace(go.Line(x=landing_ground_roll_points_output[0]['Elapsed'], y=(-1*landing_ground_roll_points_output[0]['Pusher EPU A Commanded Torque']+-1*landing_ground_roll_points_output[0]['Pusher EPU B Commanded Torque']), mode='markers', name='ground acceleration'), row=5, col=1)

    fig4.show()

    output_dataframe = pd.DataFrame()

    set_list.append(set)
    takeoff_roll_distances_list.append(landing_ground_roll_points_output[1])
    takeoff_roll_times_list.append(landing_ground_roll_points_output[2])
    # takeoff_obstacle_clearance_distances_list.append(obstacle_clearance_points_output[1])
    # takeoff_obstacle_clearance_times_list.append(obstacle_clearance_points_output[2])
    landing_roll_distances_list.append(takeoff_ground_roll_points_output[1])
    landing_roll_times_list.append(takeoff_ground_roll_points_output[2])
    # landing_obstacle_clearance_distances_list.append(ground_roll_points_output[3])
    # landing_obstacle_clearance_times_list.append(ground_roll_points_output[4])
    pattern_distance_list.append(pattern_points_output[1])
    pattern_times_list.append(pattern_points_output[2])

output_dataframe['Date of Test'] = set_list
output_dataframe['Takeoff - Ground Roll Distance'] = takeoff_roll_distances_list
output_dataframe['Takeoff - Ground Roll Time'] = takeoff_roll_times_list
# output_dataframe['Takeoff - Obstacle Clearance Distance'] = takeoff_obstacle_clearance_distances_list
# output_dataframe['Takeoff - Obstacle Clearance Time'] = takeoff_obstacle_clearance_times_list
output_dataframe['Landing - Ground Roll Distance'] = landing_roll_distances_list
output_dataframe['Landing - Ground Roll Time'] = landing_roll_times_list
# output_dataframe['Landing - Obstacle Clearance Distance'] = landing_obstacle_clearance_distances_list 
# output_dataframe['Landing - Obstacle Clearance Time'] = landing_obstacle_clearance_times_list
output_dataframe['Pattern  Distance'] = pattern_distance_list
output_dataframe['Pattern Time'] = pattern_times_list
output_dataframe.to_csv(nameout,index = False)
